# yw-linphone-android-demo : 使用 [yw-lib-linephone-android](https://bitbucket.org/yowootech/yw-lib-linphone-android/src/master/) (Sip Library)的Demo App

###此專案為Demo App，可參考此APP了解如何使用 [yw-lib-linephone-android](https://bitbucket.org/yowootech/yw-lib-linphone-android/src/master/) Sip Library ，以及如何客製化撥打流程及頁面


    
    
    

## Demo
---
####**輸入 電話帳密 / 被叫方電話 / 選擇transport，點選撥打電話，即可使用**
+ [輸入撥打電話資訊頁面](https://bitbucket.org/yowootech/yw-img/raw/master/yw-linphone-android-demo/Screenshot_20180524-082958.jpg) 
+ [撥打電話頁面](https://bitbucket.org/yowootech/yw-img/raw/master/yw-linphone-android-demo/Screenshot_20180524-083128.jpg)

    
    

## Installation 
---
####**如何將 [yw-lib-linphone-android](https://bitbucket.org/yowootech/yw-lib-linphone-android/src/master/) 引入到新專案或現有專案**

1. ####將yw-lib-linphone-android用submodule的方式，下載到專案目錄底下(為了方便管理，放在**app/libs**目錄下)，例如：
    
        git submodule add ssh://git@bitbucket.org/yowootech/yw-lib-linphone-android.git app/libs/yw-lib-linphone-android

    若成功完成載入submodule，會在主專案的git repo裡看見gitmodule的檔案更動：
    
    ![submodule的檔案更動](https://bitbucket.org/yowootech/yw-img/raw/master/readme-android-lib-linphone-1.png)


2. ####將lib專案import到主專案中：

    **File** -> **New Module** -> **Import Gradle Project**

    ![file-> new module](https://bitbucket.org/yowootech/yw-img/raw/master/readme-android-lib-linphone-2.png)
    ![import gradle project](https://bitbucket.org/yowootech/yw-img/raw/master/readme-android-lib-linphone-3.png)
    
    選擇module資源的資料夾位置：
    
    ![module resource](https://bitbucket.org/yowootech/yw-img/raw/master/readme-android-lib-linphone-4.png)
    
    成功引入後，看到專案space下出現新的lib module  
    
    ![new lib module import success](https://bitbucket.org/yowootech/yw-img/raw/master/readme-android-lib-linphone-5.png)
    
3. ####將新module加入主專案app的dependancies
    
    ![app dependencies](https://bitbucket.org/yowootech/yw-img/raw/master/readme-android-lib-linphone-6.png)
    ![select module](https://bitbucket.org/yowootech/yw-img/raw/master/readme-android-lib-linphone-7.png)
    
    此步驟完成時，會產生Error提示，會在下一步驟解決
    錯誤原因可參考此頁詳細說明：[詳細說明](https://yowootech.atlassian.net/wiki/spaces/~obvyah/pages/119832745/as+-+module+library+library+aar+git+submodule)
    
    ![error alert](https://bitbucket.org/yowootech/yw-img/raw/master/readme-android-lib-linphone-8.png)
    
4. ####於主專案中，修改build.gradle 

    在主專案的build.gradle中，repositories裡，加入一段flatDir程式碼：
    (意思是：將某個資料夾底下的檔案都攤平(flat)，gradle會在這些資料夾底下找需要的repo or aar檔等)
    (此範例中，module放在**app/libs**下面)
    
    **!! 請注意：若您的module檔案路徑不是app/libs，請記得修改以下程式碼中的路徑!! **
    
        repositories {
            jcenter()
            flatDir {
                dirs 'libs', '../app/libs/yw-lib-linphone-android/libs'
            }
        } 

    ![add code to build.gradle](https://bitbucket.org/yowootech/yw-img/raw/master/readme-android-lib-linphone-10.png)
   
5. ####可於主專案中測試看看，是否能成功引用library (主專案中可找到yw-lib-linphone-android裡面的TestUtils類別，成功！)
    
    ![test module](https://bitbucket.org/yowootech/yw-img/raw/master/readme-android-lib-linphone-14.png)

6. ####將以上步驟產生的變動，commit到主專案git中。後續下載主專案的人，只要更新到新的commit，即可順利拉下這些submodule的code

        git commit ......



## Usage
---

###**如何於新專案或現有專案，使用yw-lib-linphone-android：**

+ ####設定LinphoneConstants初始化參數: domain / outbound / expire / 撥打電話限制時間

        public class LinphoneConstants {

            public static final String LINPHONE_DOMAIN = "chiefcall.com.tw";//chief是方
            public static final String LINPHONE_OUTBOUND = "202.153.167.20";//chief是方: 正式帳號outbound
            public static final String REG_EXPIRE_TIME = "600";             //註冊sip server的expire秒數
            public static final int CALL_TIMEOUT_MIN_LIMIT = 3;
            public static final int CALL_TIMEOUT_MILLIS_LIMIT = CALL_TIMEOUT_MIN_LIMIT * 60 * 1000;//3 min
            ...
        }

+ ####新增Application Class，並在此class內進行初始化設定
        
        @Override
        public void onCreate() {
            super.onCreate();
            ...
            
            //Linphone init
            initLinphone();
        }
        
        private void initLinphone() {
            LinphoneConfig config = new LinphoneConfig.Builder()
                .setLINPHONE_DOMAIN(LinphoneConstants.LINPHONE_DOMAIN)
                .setLINPHONE_OUTBOUND(LinphoneConstants.LINPHONE_OUTBOUND)
                .setREG_EXPIRE_TIME(LinphoneConstants.REG_EXPIRE_TIME)
                .setCALL_TIMEOUT_MILLIS_LIMIT(LinphoneConstants.CALL_TIMEOUT_MILLIS_LIMIT)
                .build();

            LinphoneSetting.getInstance().init(config);
            LinphoneManager.createAndStart(this);
        }
        
+ ####新增Manifast內的Permission

    

+ ####複製及修改重要檔案及相依檔案
    + **LinphoneCoreListener** : 這是Linphone提供的core的listener, 監聽linphone core傳出來的事件（例如：重要的registrationState.callState等）。希望只在一些重要的class中使用此，不要於應用中到處註冊linphone core的listner，避免造成難以管控處理。
        目前只有CallProcess中有註冊並接收此linphone core listener事件
    + **CallProcess**：負責控制撥打電話的主流程，例如：init->註冊->撥打->撥打各狀態->撥打結束->反註冊...等。若想客製化撥打流程，修改此檔案為自己想要的流程
    + **CallActivity**：撥打電話頁面。activity內會產生一個callprocess，並委派callprocess來處理撥打電話相關流程。撥打過程的事件則透過callProcessListener的實作來接收，並呈現UI相關動作（類似MVP中，委派Presenter做事情的概念）。CallActivity只負責UI呈現及UI動作處理
    + **CallLogListener** : CallProcess會透過CallLogListener將事件及error message丟出來。有需要可過CallLogListener做log相關的處理。
    ![UML](https://bitbucket.org/yowootech/yw-img/raw/master/yw-linphone-android-demo/call-uml.jpg)